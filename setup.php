<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('configs/config.php');
require('library/library.php');

// load Smarty library
require('includes/smarty/libs/Smarty.class.php');

// The setup.php file is a good place to load
// required application library files, and you
// can do that right here. An example:
// require('guestbook/guestbook.lib.php');

class Smarty_pokebattle extends Smarty {

   function __construct()
   {

        // Class Constructor.
        // These automatically get set with each new instance.

        parent::__construct();

        $this->setTemplateDir('C:/MAMP/htdocs/jaar_2/pokebattle/templates/');
        $this->setCompileDir('C:/MAMP/htdocs/jaar_2/pokebattle/templates_c/');
        $this->setConfigDir('C:/MAMP/htdocs/jaar_2/pokebattle/configs/');
        $this->setCacheDir('C:/MAMP/htdocs/jaar_2/pokebattle/cache/');

        // $this->caching = Smarty::CACHING_LIFETIME_CURRENT;
        $this->force_compile = true;

        $this->assign('name', 'pokebattle');
   }

}
?>