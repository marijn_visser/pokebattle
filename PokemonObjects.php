<?php

// pokemon class begin
class Pokemon
{
    // private objects
    private $name;
    private $energyType;
    private $health;
    private $attacks = array();
    private $speed;
    private $weakness;
    private $weaknessMulti;
    private $resistance;

    public function __construct($name, $energyType, $health, $attacks, $speed, $resistance, $weakness, $weaknessMulti) {
        $this->name = $name;
        $this->energyType = $energyType;
        $this->health = $health;
        $this->speed = $speed;
        $this->resistance = $resistance;
        $this->weakness = $weakness;
        $this->weaknessMulti = $weaknessMulti;
        foreach($attacks as $attack ){
            $this->attacks[] = new skill($attack);    
        }
        
    }
    public function getName() {
        return $this->name;
    }
    public function getEnergyType() {
        return $this->energyType;
    }
    public function getHealth() {
        return $this->health;
    }
    public function getAttacks() {
        return $this->attacks;
    }
    public function getSpeed() {
        return $this->speed;
    }
    public function getResistance() {
        return $this->resistance;
    }
    public function getWeakness() {
        return $this->weakness;
    }
    public function getWeaknessMulti(){
        return $this->weaknessMulti;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setHealth($health) {
        $this->health = $health;
    }
    function setSpeed($speed) {
        $this->speed = $speed;
    }

    // attack function begin
    public function attack($attack,$enemy){
        $message = ''; 
        $damage = $attack->getDamage();
        if($attack->getType() == $enemy->getWeakness()){
        $damage = $damage * 2;
        $message.= " He dealt alot of damage <br> ";
        }
        elseif($attack->getType() == $enemy->getResistance()){
            $damage = $damage * 0.5;
            $message.= " Pikachu dealt no damage <br> ";
        }   
        $message.= " dealt damage ". $damage . "<br>";  
        $message.= " HP ". $enemy->getHealth(). "<br>"; 
        $damage =round($damage);
        $enemy->dealDamage($damage);
            return $message ;
        }

        public function dealDamage($damage){
            $this->health -= $damage;
        }
}
// attack function end
// pokemon class end


// skill class begin
class skill
{
    
    private $name;
    private $damage;
    private $type;

    public function __construct($attack){
        $this->name = $attack['name'];
        $this->damage = $attack['damage'];
        $this->type = $attack['type'];

    }

    public function getName() {
        return $this->name;
    }
    public function getDamage() {
        return $this->damage;
    }
    public function getType() {
        return $this->type;
    }
    
    
}

// Skills class end


// inheritance begin
class  electric extends Pokemon{
    private $voltage;
    
    public function __construct($name, $energyType, $health, $attacks, $speed, $resistance, $weakness, $weaknessMulti, $voltage){
        parent::__construct($name, $energyType, $health, $attacks, $speed, $resistance, $weakness, $weaknessMulti);
        $this->voltage = $voltage;
    }
    public function getVoltage(){
        return $this->voltage;
    }
    
}

// inheritance end

?>